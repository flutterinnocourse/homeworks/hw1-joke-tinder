import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:swipe/swipe.dart';
import 'package:swipe_detector/domain/best_jokes.dart';
import 'package:swipe_detector/domain/current_category.dart';
import 'package:swipe_detector/ui/categories_picker.dart';
import 'package:swipe_detector/ui/search_page.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:hive_flutter/hive_flutter.dart';

import 'package:swipe_detector/domain/joker.dart';
import 'package:swipe_detector/data/joke_info.dart';
import 'package:swipe_detector/ui/best_jokes_page.dart';

void main() async {
  await Hive.initFlutter();
  Hive.registerAdapter(JokeInfoAdapter());
  await Hive.openBox<JokeInfo>('jokeBox');
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Swipe Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const SwipePage(title: 'Joke Tinder'),
    );
  }
}

class SwipePage extends StatefulWidget {
  const SwipePage({super.key, required this.title});
  final String title;

  @override
  SwipePageState createState() => SwipePageState();
}

class SwipePageState extends State<SwipePage> {
  String _joke = "Kolobok povesilsya";
  String _url = "https://otvet.mail.ru/question/44046516";
  String _id = "0";

  late final Box box;

  Future<void> _getNewJoke(String categoryValue) async {
    final Joker joker = Joker.filled();
    final JokeInfo jokeInfo = await joker.getRandomJoke(categoryValue);
    if (jokeInfo.url != "") {
      setState(() {
        _joke = jokeInfo.joke;
        _url = jokeInfo.url;
        _id = jokeInfo.id;
      });
    }
  }

  Future<void> _addToTheBestList(BestJokes bestJokes) async {
    // add to the best
    final JokeInfo jokeInfo = JokeInfo(joke: _joke, url: _url, id: _id);
    bestJokes.add(jokeInfo);
    box.add(jokeInfo);
  }

  Future<void> _onOpen() async {
    Uri link = Uri.parse(Uri.encodeFull(_url));
    if (await canLaunchUrl(link)) {
      await launchUrl(link);
    } else {
      log('Could not launch $link');
    }
  }

  @override
  void initState() {
    super.initState();
    // Get reference to an already opened box
    box = Hive.box<JokeInfo>('jokeBox');
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
            create: (_) => BestJokes(box.values.cast<JokeInfo>().toList())),
        ChangeNotifierProvider(create: (_) => CurrentCategory())
      ],
      child: Consumer2<BestJokes, CurrentCategory>(
        builder: (context, bestJokes, currentCategory, child) => Scaffold(
          appBar: AppBar(
            // Here we take the value from the MyHomePage object that was created by
            // the App.build method, and use it to set our appbar title.
            title: Text(widget.title),
          ),
          body: SafeArea(
            minimum: const EdgeInsets.all(20),
            child: Swipe(
              child: Container(
                color: Colors.white,
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: Center(
                    child: SingleChildScrollView(
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                      CategoriesPicker(currentCategory: currentCategory),
                      Card(
                          color: Colors.blue,
                          child: Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Text(_joke,
                                style: const TextStyle(
                                  color: Colors.white,
                                  fontSize: 26,
                                )),
                          )),
                      Container(
                          margin: const EdgeInsets.all(5),
                          child: ElevatedButton(
                            onPressed: _onOpen,
                            child: const Text(
                              'Open Source',
                              style: TextStyle(fontSize: 20.0),
                            ),
                          )),
                      Row(
                        children: [
                          Container(
                              margin: const EdgeInsets.all(10),
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    side: const BorderSide(
                                        width: 3, color: Colors.black),
                                    shape: RoundedRectangleBorder(
                                        //to set border radius to button
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    padding: const EdgeInsets.all(
                                        20) //content padding inside button
                                    ),
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => const SearchPage(
                                            title: "Search Joke")),
                                  );
                                },
                                child: const Text(
                                  'Search',
                                  style: TextStyle(fontSize: 20.0),
                                ),
                              )),
                          Container(
                              margin: const EdgeInsets.all(10),
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    side: const BorderSide(
                                        width: 3, color: Colors.black),
                                    shape: RoundedRectangleBorder(
                                        //to set border radius to button
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    padding: const EdgeInsets.all(
                                        20) //content padding inside button
                                    ),
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => BestJokesPage(
                                            bestJokes: bestJokes)),
                                  );
                                },
                                child: const Text(
                                  'Favourites',
                                  style: TextStyle(fontSize: 20.0),
                                ),
                              ))
                        ],
                      )
                    ]))),
              ),
              onSwipeLeft: () {
                setState(() {
                  _getNewJoke(currentCategory.value);
                });
              },
              onSwipeRight: () {
                setState(() {
                  _getNewJoke(currentCategory.value);
                });
              },
              onSwipeUp: () {
                setState(() {
                  _addToTheBestList(bestJokes);
                  _getNewJoke(currentCategory.value);
                });
              },
            ),
          ),
        ),
      ),
    );
  }
}

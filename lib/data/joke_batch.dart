import 'package:json_annotation/json_annotation.dart';
import 'package:swipe_detector/data/joke_info.dart';

part 'joke_batch.g.dart';

@JsonSerializable()
class JokeBatch {
  final int total;

  @JsonKey(name: 'result')
  final List<JokeInfo> jokes;

  JokeBatch({required this.total, required this.jokes});

  factory JokeBatch.fromJson(Map<String, dynamic> json) =>
      _$JokeBatchFromJson(json);

  Map<String, dynamic> toJson() => _$JokeBatchToJson(this);
}

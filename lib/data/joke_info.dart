import 'package:json_annotation/json_annotation.dart';
import 'package:hive/hive.dart';

part 'joke_info.g.dart';

@JsonSerializable()
@HiveType(typeId: 0)
class JokeInfo {
  @HiveField(0)
  @JsonKey(name: 'value')
  final String joke;

  @HiveField(1)
  final String url;

  @HiveField(2)
  final String id;

  JokeInfo({required this.joke, required this.url, required this.id});

  factory JokeInfo.fromJson(Map<String, dynamic> json) =>
      _$JokeInfoFromJson(json);

  Map<String, dynamic> toJson() => _$JokeInfoToJson(this);
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'joke_batch.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JokeBatch _$JokeBatchFromJson(Map<String, dynamic> json) => JokeBatch(
      total: json['total'] as int,
      jokes: (json['result'] as List<dynamic>)
          .map((e) => JokeInfo.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$JokeBatchToJson(JokeBatch instance) => <String, dynamic>{
      'total': instance.total,
      'result': instance.jokes,
    };

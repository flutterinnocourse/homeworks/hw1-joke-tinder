// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'joke_info.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class JokeInfoAdapter extends TypeAdapter<JokeInfo> {
  @override
  final int typeId = 0;

  @override
  JokeInfo read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return JokeInfo(
      joke: fields[0] as String,
      url: fields[1] as String,
      id: fields[2] as String,
    );
  }

  @override
  void write(BinaryWriter writer, JokeInfo obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.joke)
      ..writeByte(1)
      ..write(obj.url)
      ..writeByte(2)
      ..write(obj.id);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is JokeInfoAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JokeInfo _$JokeInfoFromJson(Map<String, dynamic> json) => JokeInfo(
      joke: json['value'] as String,
      url: json['url'] as String,
      id: json['id'] as String,
    );

Map<String, dynamic> _$JokeInfoToJson(JokeInfo instance) => <String, dynamic>{
      'value': instance.joke,
      'url': instance.url,
      'id': instance.id,
    };

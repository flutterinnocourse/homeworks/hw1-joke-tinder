import 'package:flutter/material.dart';
import 'package:swipe_detector/domain/current_category.dart';

class CategoriesPicker extends StatefulWidget {
  const CategoriesPicker({super.key, required this.currentCategory});

  final CurrentCategory currentCategory;

  @override
  State<CategoriesPicker> createState() => _CategoriesPickerState();
}

class _CategoriesPickerState extends State<CategoriesPicker> {
  final List<String> categories = [
    "any",
    "animal",
    "career",
    "celebrity",
    "dev",
    "explicit",
    "fashion",
    "food",
    "history",
    "money",
    "movie",
    "music",
    "political",
    "religion",
    "science",
    "sport",
    "travel"
  ];

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      value: widget.currentCategory.value,
      icon: const Icon(Icons.arrow_downward),
      elevation: 16,
      style: const TextStyle(color: Colors.blue),
      underline: Container(
        height: 2,
        color: Colors.blue,
      ),
      onChanged: (String? value) {
        // This is called when the user selects an item.
        setState(() {
          widget.currentCategory.value = value!;
        });
      },
      items: categories.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }
}

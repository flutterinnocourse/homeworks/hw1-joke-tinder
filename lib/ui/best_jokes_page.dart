import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:swipe_detector/domain/best_jokes.dart';

class BestJokesPage extends StatefulWidget {
  const BestJokesPage({super.key, required this.bestJokes});

  final BestJokes bestJokes;

  @override
  State<BestJokesPage> createState() => _BestJokesPageState();
}

class _BestJokesPageState extends State<BestJokesPage> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: widget.bestJokes,
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Favourite Jokes'),
        ),
        body: _JokeList(),
      ),
    );
  }
}

class _JokeList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var jokeList = Provider.of<BestJokes>(context);

    return ListView.separated(
      padding: const EdgeInsets.all(8),
      itemCount: jokeList.jokes.length,
      itemBuilder: (BuildContext context, int index) {
        return Card(
            color: Colors.blue,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(jokeList.jokes[index].joke,
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 26,
                  )),
            ));
      },
      separatorBuilder: (BuildContext context, int index) => const Divider(),
    );
  }
}

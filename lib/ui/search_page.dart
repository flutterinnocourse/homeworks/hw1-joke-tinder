import 'package:flutter/material.dart';
import 'package:swipe_detector/data/joke_batch.dart';
import 'package:swipe_detector/data/joke_info.dart';
import 'package:swipe_detector/domain/joker.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({super.key, required this.title});
  final String title;

  @override
  MyHomePageState createState() => MyHomePageState();
}

class MyHomePageState extends State<SearchPage> {
  TextEditingController editingController = TextEditingController();
  List<JokeInfo> jokes = [];

  @override
  void initState() {
    super.initState();
  }

  Future<void> _searchJokes(String searchQuery) async {
    final Joker joker = Joker.filled();
    final JokeBatch jokeBatch = await joker.searchJokes(searchQuery);
    setState(() {
      jokes = jokeBatch.jokes;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              onChanged: (value) {
                _searchJokes(value);
              },
              controller: editingController,
              decoration: const InputDecoration(
                  labelText: "Search",
                  hintText: "Search",
                  prefixIcon: Icon(Icons.search),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(25.0)))),
            ),
          ),
          Expanded(
              child: ListView.separated(
            padding: const EdgeInsets.all(8),
            itemCount: jokes.length,
            itemBuilder: (BuildContext context, int index) {
              return Card(
                  color: Colors.blue,
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Text(jokes[index].joke,
                        style: const TextStyle(
                          color: Colors.white,
                          fontSize: 26,
                        )),
                  ));
            },
            separatorBuilder: (BuildContext context, int index) =>
                const Divider(),
          )),
        ],
      ),
    );
  }
}

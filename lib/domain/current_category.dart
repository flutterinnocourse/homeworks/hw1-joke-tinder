import 'package:flutter/cupertino.dart';

class CurrentCategory extends ChangeNotifier {
  String _value = "any";

  String get value => _value;

  set value(String newValue) {
    _value = newValue;
    notifyListeners();
  }
}

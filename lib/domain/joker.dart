import 'package:dio/dio.dart';
import 'package:swipe_detector/data/joke_batch.dart';
import 'package:swipe_detector/data/joke_info.dart';

const String _baseUrl = 'https://api.chucknorris.io';

class Joker {
  factory Joker.filled() {
    final Dio dio = Dio(BaseOptions(
      baseUrl: _baseUrl,
    ));
    return Joker._(dio);
  }

  Joker._(this._dio);

  final Dio _dio;

  Future<JokeInfo> getRandomJoke(String categoryValue) async {
    String endpoint = categoryValue == "any"
        ? '/jokes/random'
        : "/jokes/random?category=$categoryValue";
    final Response<dynamic> response = await _dio.get(endpoint);
    JokeInfo jokeInfo = response.statusCode == 200
        ? JokeInfo.fromJson(response.data)
        : JokeInfo(joke: "", url: "", id: "");
    return jokeInfo;
  }

  Future<JokeBatch> searchJokes(String searchQuery) async {
    String endpoint =
        "https://api.chucknorris.io/jokes/search?query=$searchQuery";
    final Response<dynamic> response = await _dio.get(endpoint);
    JokeBatch jokeBatch = response.statusCode == 200
        ? JokeBatch.fromJson(response.data)
        : JokeBatch(jokes: [], total: 0);
    return jokeBatch;
  }
}

import 'package:flutter/cupertino.dart';
import 'package:swipe_detector/data/joke_info.dart';

class BestJokes extends ChangeNotifier {
  BestJokes(List<JokeInfo> jokes) {
    _jokes = jokes;
  }
  late List<JokeInfo> _jokes;

  List<JokeInfo> get jokes => _jokes;

  void add(JokeInfo jokeInfo) {
    _jokes.add(jokeInfo);
    notifyListeners();
  }
}

# Joke Tinder

This application allows you to read some jokes using swipes. 

Moreover it allows you to go to the source of joke.

## Screencast

Demonstration of the work :

<img src="joke_tinder_demo.gif" width="300" height="600" />

## Install

All dependencies in : `pubspec.yaml`

This application was succesfully tested on Google Pixel 5 (Android 13)

You can download APK : `app-release.apk` 

## Changelog

### HW2

* Swipe-UP to add to favorite jokes
* Add favorite jokes screen
* State management using Provider-Consumer
* Store favorite jokes in hive
* Choose joke category
* Extra screen for searching joke by query

### HW1

* Main screen
* Random joke client-server
* Swipe handling
* Open source of the joke
